# Hyperledger Fabric and Composer Setup

<h6>The project is tested on - </h6>
<p>OS       : Ubuntu 16.04 LTS</p>
<p>IDE used : Visual Studio Code</p>

<h5>Software requirement and Installation steps</h5>

1. Visual Studio Code - with docker and Hyperledger Composer extension

2. Install Node (node version 9 is not supported). Run command "node -v" to check correct installation and version of node.

3. Git (version 2.9 or higher) "git -version"

4. Python (version 2.7.x higher), Add Python to PATH. Run "python -V"

5. Yeoman using NPM

6. Composer CLI using NPM. Run the command - "npm install -g composer-cli" and then "composer -v"

7. Composer REST server. Run the command - "npm install -g composer-rest-server" and then "composer-rest-server-v"

8. Yeoman generator - Run the command - "npm install -g generator-hyperledger-composer" and then " yo --generators"

<p>Please make a note of directory of all installations. It will be required later.

Hyperledger Fabric framework is installed on our machine in the form of Docker image.</p>

<p>Open Hyper-V Manager --> Start Ubuntu VM</p>

<h5>On Ubuntu:</h5>

1. Install Hyperledger composer by running CURL command given in the docs. Curl -o https://hyperledger.github.io/composer/prereqs-ubuntu.sh

2. Chmod u+x prereqs-ubuntu.sh

3. Run the script - ./prereqs-ubuntu.sh

4. Install composer CLI tool using npm "npm install -g composer-cli"

5. Install Hyperledger Fabric. Copy the script from docs -section - Installing the dev env - step 4..

6. Unzip the downloaded Fabric folder

<h5>Dev Env is ready now! Now run the Fabric </h5>

1. To execute the Fabric for the first time, downloaded of all docker images is required. Run the script - ./downloadFabric.sh

2. Docker images

3. ./startFabric.sh

4. Docker ps - to check all containers that are up

5. ./createPeerAdminCard.sh - to create a card to connect to Fabric. 

