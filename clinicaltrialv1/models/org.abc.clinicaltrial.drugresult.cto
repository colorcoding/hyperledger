namespace org.abc.clinicaltrial.drugresult

import org.abc.clinicaltrial.drug.Drug

asset DrugResult identified by drugResultId {
  o String drugResultId

  // Refers to the Drug Asset
  o String drugId

  o DateTime courseStartDate optional

  o DateTime courseEndDate optional

  o DateTime actualCourseEndDate optional

  o String physicianId

  o String patientId optional

  o TrialResultStatus result default="NA" optional

  o CourseStatus courseStatus default="NOTSTARTED" optional
}

enum TrialResultStatus {
  o PASS
  o FAIL
  o NA
}

enum CourseStatus {
  o NOTSTARTED
  o STARTED
  o ENDED
}

/** */
// CtPharma will create a new Drug Result
// An event is also associated
transaction CreateNewDrugResult {
  o String drugId
  o String physicianId
}

// CtPhysician would be subscribed
event NewDrugResultCreated{
  o String drugResultId
}
/** */

/** */
// CtPhysician will change the status to STARTED
// Associate to a Patient
// Check if physicianId is same as the partipant 
// who is doing this txn
transaction StartTheTrial{
  o Drug drug
  o String patientId
  o DateTime courseStartDate
  o DateTime courseEndDate
  o CourseStatus courseStatus
  o TrialResultStatus resultStatus
}

event TrialStarted{
  o String patientId
}
/** */

/** */
// CtPatient will end the trial and change
// The courseStatus and ResultStatus
// Check if patientId is same as the participant
// who is doing this Txn
transaction EndTheTrial {
  o String patientId
  o DateTime actualCourseEndDate
  o CourseStatus courseStatus
  o TrialResultStatus resultStatus
}
event TrialEnded {
  o TrialResultStatus resultStatus
}
/** */