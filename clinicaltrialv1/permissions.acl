rule NetworkRealAllSystem {
  description:"All participants can READ the system registries"
  participant:"ANY"
  operation:READ
  resource:"org.hyperledger.composer.system.*"
  action:ALLOW
}

//NetworkAdmin can carry out Identities Management
// And Network operations
rule NetworkAdminControlPermission {
  description:"NetworkAdmin has all permissions"
  participant:"org.hyperledger.composer.system.NetworkAdmin"
  operation: ALL
  resource:"org.hyperledger.composer.system.*"
  action:ALLOW
}

//NetworkAdmin can control Ct Participants
rule NetworkRegistryControlPermission{
  description:"NetworkAdmin has all permissions on CtParticipants"
  participant:"org.hyperledger.composer.system.NetworkAdmin"
  operation: ALL
  resource:"org.abc.clinicaltrial.participant.*"
  action:ALLOW
}

rule CtParticipantsReadAll {
  description:"Ct Participants can read all resources"
  participant:"org.abc.clinicaltrial.participant.CtParticipant"
  operation: READ
  resource:"org.abc.clinicaltrial.**"
  action:ALLOW
}

rule CtParticipantsCreateTransaction {
  description:"All 3 Ct participants - Pharma, Physician and Patient can create a transaction"
  participant:"org.abc.clinicaltrial.participant.CtParticipant"
  operation: CREATE
  resource:"org.hyperledger.composer.system.HistorianRecord"
  action:ALLOW
}

//Giving more fine grained permissions now

// Only Pharma company can create a Drug
rule createDrugPermission {
  description:"Only Pharma can create a Drug"
  participant:"org.abc.clinicaltrial.participant.CtPharma"
  operation: CREATE
  resource:"org.abc.clinicaltrial.drugresult.*"
  transaction:"org.abc.clinicaltrial.drugresult.CreateNewDrugResult"
  action:ALLOW
}

// Only Physician can start the trial
rule startTheTrial{
  description:"Only Physician can start the trial i.e. update the DrugResult and associate Patient with it"
  participant:"org.abc.clinicaltrial.participant.CtPhysician"
  operation:UPDATE
  resource:"org.abc.clinicaltrial.drugresult.*"
  transaction:"org.abc.clinicaltrial.drugresult.StartTheTrial"
  action:ALLOW
}

// Only Patient can start the trial
rule EndTheTrial{
  description:"Only Patient can end the trial by updating the trial status"
  participant:"org.abc.clinicaltrial.participant.CtPatient"
  operation:UPDATE
  resource:"org.abc.clinicaltrial.drugresult.*"
  transaction:"org.abc.clinicaltrial.drugresult.EndTheTrial"
  action:ALLOW
}
