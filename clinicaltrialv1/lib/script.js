/**
 * Create a new Drug Result
 * @param {org.abc.clinicaltrial.drugresult.CreateNewDrugResult} drugResult
 * @transaction
 */
function createNewDrugResult(drugResultInput) {
    var currentTime = new Date().getTime();
    var currentDate = new Date().getDate();

    return getAssetRegistry('org.abc.clinicaltrial.drugresult.DrugResult')
        .then(function(drugResultRegistry){
            var factory = getFactory();
            var namespace = 'org.abc.clinicaltrial.drugresult';

              // Verify if drugId is valid use the query
            

            var drugResultId = generateNewDrugResultId(drugResultInput.drugId, drugResultInput.physicianId);
            var drugResult = factory.newResource(namespace, 'DrugResult', drugResultId);
      		drugResult.drugId = drugResultInput.drugId;
      		drugResult.physicianId = drugResultInput.physicianId;
      
            // Emit Event
            var event = factory.newEvent(namespace, 'NewDrugResultCreated');
            event.drugResultId = drugResultId;
            emit(event);
      		
        	console.log("event emitted");
            // Add transaction to registry
            return drugResultRegistry.add(drugResult);
        });
}

/**
 * Start the trial
 * @param {org.abc.clinicaltrial.drugresult.StartTheTrial} drugResult
 * @transaction
 */
function startTheTrial(drugResultInput) {
    var currentTime = new Date().getTime();
    var currentDate = new Date().getDate();

    return getAssetRegistry('org.abc.clinicaltrial.drugresult.DrugResult')
        .then(function(drugResultRegistry){
            return drugResultRegistry.get(drugResultInput.drugResultId);
        }).then(function(drugResult){
            // Do the update
            var factory = getFactory();
            drugResult.patientId = drugResultInput.patientId;
            drugResult.courseStartDate = currentDate;
            drugResult.courseStatus = "STARTED";
            return drugResultRegistry.update(drugResult);
        }).then(function(){
            // Emit Event
            var event = factory.newEvent(namespace, 'TrialStarted');
            event.patientId = drugResultInput.patientId;
            emit(event);
            console.log("event emitted");
        }).catch(function(error){
            throw new Error(error);
        });
}

/**
 * Using random number to generate unique drugResultId
 * Later we can query the registry/ use incremental function
 * @param {*} drugId 
 * @param {*} physicianId 
 */
function generateNewDrugResultId(drugId, physicianId) {
    var currentTime = new Date().getTime();
    var currentDate = new Date().getDate();
    // Generate random number between 0-100
    var random = Math.floor(Math.random() * 100);
    return drugId + '_' + physicianId + '_' + currentDate + '_' + currentTime;
}